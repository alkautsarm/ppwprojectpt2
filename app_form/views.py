from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .forms import KegiatanForm
from .models import Kegiatan

def kegiatan_forms(request):
    form_construct = KegiatanForm(request.POST or None)
    response = {'form':form_construct.as_p()}
    
    if (request.method == 'POST' and form_construct.is_valid()) :
    	nama_kegiatan = form_construct.cleaned_data['kegiatanform_nama']
    	kategori_kegiatan = form_construct.cleaned_data['kegiatanform_kategori']
    	waktu_kegiatan = form_construct.cleaned_data['kegiatanform_waktu']
    	tempat_kegiatan = form_construct.cleaned_data['kegiatanform_tempat']

    	kegiatan = Kegiatan(kegiatan_nama = nama_kegiatan, kegiatan_kategori = kategori_kegiatan,
    		kegiatan_waktu = waktu_kegiatan, kegiatan_tempat = tempat_kegiatan)
    	kegiatan.save()
    return render(request, 'form.html', response)

def kegiatan_display(request):
    list_kegiatan = Kegiatan.objects.all().values()
    response = {'display':list_kegiatan}

    return render(request, 'display.html', response)

def delete_jadwal(request):
    kegiatan = Kegiatan.objects.all()
    kegiatan.delete()

    return redirect('display_jadwal')
    