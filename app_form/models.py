from django.db import models

# Create your models here.
class Kegiatan(models.Model) :
	kegiatan_nama = models.CharField(max_length = 30)
	kegiatan_kategori = models.CharField(max_length = 30)
	kegiatan_waktu = models.DateTimeField()
	kegiatan_tempat = models.CharField(max_length = 80)