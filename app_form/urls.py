from django.urls import path

from . import views

urlpatterns = [
	path('tambah-kegiatan', views.kegiatan_forms, name='forms_kegiatan'), #path(route, view, ...)
	path('jadwal-kegiatan', views.kegiatan_display, name='display_jadwal'), #path(route, view, name panggilan di views & html)
	path('jadwal-kegiatan-deleted', views.delete_jadwal, name='delete_jadwal'), #path(route, view, ...)
]