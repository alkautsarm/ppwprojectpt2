from django import forms

class KegiatanForm(forms.Form):
	kegiatanform_nama = forms.CharField(label='Nama', max_length = 30, 
		widget=forms.TextInput(attrs={'placeholder':'Contoh: Rapat BEM', 'style':'min-width:20vw'}))

	kegiatanform_kategori = forms.CharField(label='Kategori', max_length = 30, 
		widget=forms.TextInput(attrs={'placeholder':'Contoh: Kepanitiaan', 'style':'min-width:20vw'}))

	kegiatanform_waktu = forms.DateTimeField(label='Tanggal & Waktu', input_formats=['%Y-%m-%dT%H:%M'],
		widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))

	kegiatanform_tempat = forms.CharField(label='Tempat', max_length = 80, 
		widget=forms.TextInput(attrs={'placeholder':'Contoh: Ruang BEM Fasilkom UI', 'style':'min-width:20vw'}))