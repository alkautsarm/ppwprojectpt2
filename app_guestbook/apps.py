from django.apps import AppConfig


class AppGuestbookConfig(AppConfig):
    name = 'app_guestbook'
